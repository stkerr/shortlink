package bot_type

import "github.com/batazor/shortlink/internal/notify"

var (
	METHOD_NEW_LINK      = notify.NewEventID()
	METHOD_SEND_NEW_LINK = notify.NewEventID()
)
