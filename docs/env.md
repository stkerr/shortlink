<!---
File generated by cli. DO NOT EDIT.
-->

|Name | Default Value | Description |
|---|---|---|
| "BOT_SLACK_WEBHOOK" | YOUR_WEBHOOK_URL_HERE | Your webhook URL |
| "BOT_SMTP_FROM" | example@site.com |  |
| "BOT_SMTP_PASS" | YOUR_PASSWORD |  |
| "BOT_SMTP_TO" | EMAIL_USER |  |
| "BOT_SMTP_HOST" | smtp.gmail.com |  |
| "BOT_SMTP_ADDR" | smtp.gmail.com:587 |  |
| "BOT_TELEGRAM_WEBHOOK" | YOUR_WEBHOOK_URL_HERE | Your webhook URL |
| "BOT_TELEGRAM_CHAT_ID" | 123 | Your chat ID |
| "BOT_TELEGRAM_DEBUG_MODE" | false | Debug mode |
| "LOG_LEVEL" | logger.INFO_LEVEL |  |
| "LOG_TIME_FORMAT" | time.RFC3339Nano |  |
| "TRACER_SERVICE_NAME" | ShortLink | Service Name |
| "TRACER_URI" | localhost:6831 | Tracing addr:host |
| "MQ_ENABLED" | false | Enabled MQ-service |
| "SENTRY_DSN" | __DSN__ | key for sentry |
| "GRPC_SERVER_PORT" | 50051 | gRPC port |
| "GRPC_CLIENT_PORT" | 50051 | gRPC port |
| "LOG_LEVEL" | logger.INFO_LEVEL |  |
| "LOG_TIME_FORMAT" | time.RFC3339Nano |  |
| "TRACER_SERVICE_NAME" | ShortLink |  |
| "TRACER_URI" | localhost:6831 |  |
| "MQ_ENABLED" | false |  |
| "SENTRY_DSN" | __DSN__ |  |
| "GRPC_SERVER_PORT" | 50051 |  |
| "GRPC_CLIENT_PORT" | 50051 |  |
| "MQ_TYPE" | rabbitmq | Select: kafka, rabbitmq, nats |
| "MQ_KAFKA_URI" | localhost:9092 | Kafka URI |
| "MQ_KAFKA_CONSUMER_GROUP" | shortlink | Kafka consumer group |
| "MQ_RABBIT_URI" | amqp://localhost:5672 | RabbitMQ URI |
| "STORE_TYPE" | ram | Select: postgres, mongo, mysql, redis, dgraph, sqlite, leveldb, badger, ram, scylla, cassandra |
| "STORE_BADGER_PATH" | /tmp/links.badger | Badger path to file |
| "STORE_CASSANDRA_URI" | localhost:9042 | Cassandra URI |
| "STORE_DGRAPH_URI" | localhost:9080 | DGRAPH URI |
| "STORE_LEVELDB_PATH" | /tmp/links.db | LevelDB path to file |
| "STORE_MONGODB_URI" | mongodb://localhost:27017/shortlink | MongoDB URI |
| "STORE_MODE_WRITE" | storeOptions.MODE_SINGLE_WRITE | mode write to store |
| "STORE_MYSQL_URI" | shortlink:shortlink@(localhost:3306)/shortlink?parseTime=true | MySQL URI |
| "STORE_POSTGRES_URI" | postgres://shortlink:shortlink@localhost:5435/shortlink?sslmode=disable | Postgres URI |
| "STORE_MODE_WRITE" | storeOptions.MODE_SINGLE_WRITE | mode write to store |
| "STORE_MODE_WRITE" | options.MODE_SINGLE_WRITE | mode write to store |
| "STORE_REDIS_URI" | localhost:6379 | Redis URI |
| "STORE_RETHINKDB_URI" | localhost:28015 | RethinkDB URI |
| "STORE_SCYLLA_URI" | localhost:9042 | Scylla URI |
| "STORE_SQLITE_PATH" | /tmp/links.sqlite | SQLite URI |
| "API_TYPE" | http-chi | Select: http-chi, gRPC-web, graphql, cloudevents, go-kit |
| "API_PORT" | 7070 | API port |
| "API_TIMEOUT" | 60 | Request Timeout |
