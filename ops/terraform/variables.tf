variable "psql-user" {
  default = "postgres"
  description = "Default PostgreSQL user"
}

variable "psql-password" {
  default = "postgres"
  description = "Default PostgreSQL password"
}

variable "psql-database" {
  default = "postgres"
  description = "Default PostgreSQL database"
}
